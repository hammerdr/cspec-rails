require 'rails'

module CSpec
  module Rails
    class Railtie < ::Rails::Railtie
      rake_tasks do
        load 'cspec-rails/tasks/cspec.rake'
      end
    end
  end
end
