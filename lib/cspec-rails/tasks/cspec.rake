task :default => :cspec
desc "Run all coffeescript specs"
task :cspec => "cspec:run"

namespace :cspec do
  desc "Run all coffeescript specs"
  task :run do
    files = FileList.new("spec/**/*_spec.coffee", "spec/*_spec.coffee").to_a
    puts `cspec -J --include app/assets/javascripts #{files.join(" ")}`
  end
end
